const commandLineArgs = require("command-line-args");
const { Api, JsonRpc, RpcError } = require("eosjs");
const fetch = require("node-fetch"); // node only; not needed in browsers

const rpc = new JsonRpc("https://api.telos.kitchen", { fetch });

async function loadOptions() {
  const optionDefinitions = [
    { name: "blockproducer", alias: "p", type: String },
    { name: "rankThreshold", alias: "r", type: String },
  ];

  return commandLineArgs(optionDefinitions);
}

async function main () {
  const opts = await loadOptions();

  let producer_table = await rpc.get_table_rows({
    json: true,
    code: 'eosio',
    scope: 'eosio',
    table: 'producers',
    limit: 100
  });

  let active_producers = producer_table.rows.filter(function (el) {
    return el.is_active == 1;
  });

  active_producers.sort((a, b) => parseFloat(b.total_votes) - parseFloat(a.total_votes));

  let rank = -1;
  let count = 1;
  for(let producer of active_producers) {
    if (producer.owner == opts.blockproducer) {
      rank = count;
    }
    count++;
  }

  console.log ("Producer rank of ", opts.blockproducer, " : ", rank);  

  var Plugin = require ('nagios-plugin');
  var o = new Plugin ({
    shortName : 'bp_rank'
  });

  console.log ('Opts.rank :', opts.rankThreshold)
  const criticalLevel = parseInt(opts.rankThreshold) + 1;
  console.log ('Critical Level: ', criticalLevel);


  o.setThresholds({
    'critical' : parseInt(opts.rankThreshold) + 1,
    'warning' : parseInt(opts.rankThreshold)
  });
  console.log (o.threshold)

  var state = o.checkThreshold (rank);
  o.addMessage(state, "Producer rank of " + opts.blockproducer + " : " + rank );
  o.addPerfData({
    label: "rank",
    value: rank,
    threshold: o.threshold,
    uom : "th place",
    min : 1
  });

  var messageObj = o.checkMessages();
  console.log(o)
  o.nagiosExit(messageObj.state, messageObj.message);

}


main ();